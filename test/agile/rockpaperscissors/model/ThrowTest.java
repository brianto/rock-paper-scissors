package agile.rockpaperscissors.model;

import junit.framework.Assert;

import org.junit.Test;

public class ThrowTest {
	private static final Throw ROCK = Throw.ROCK;
	private static final Throw PAPER = Throw.PAPER;
	private static final Throw SCISSORS = Throw.SCISSORS;
	
	private static final ThrowResult WIN = ThrowResult.WIN;
	private static final ThrowResult LOSS = ThrowResult.LOSS;
	private static final ThrowResult DRAW = ThrowResult.DRAW;
	
	@Test
	public void testRockPaperScissorsThrowResults() {
		Assert.assertEquals(DRAW, ROCK.resultAgainst(ROCK));
		Assert.assertEquals(LOSS, ROCK.resultAgainst(PAPER));
		Assert.assertEquals(WIN, ROCK.resultAgainst(SCISSORS));
		
		Assert.assertEquals(WIN, PAPER.resultAgainst(ROCK));
		Assert.assertEquals(DRAW, PAPER.resultAgainst(PAPER));
		Assert.assertEquals(LOSS, PAPER.resultAgainst(SCISSORS));
		
		Assert.assertEquals(LOSS, SCISSORS.resultAgainst(ROCK));
		Assert.assertEquals(WIN, SCISSORS.resultAgainst(PAPER));
		Assert.assertEquals(DRAW, SCISSORS.resultAgainst(SCISSORS));
	}
}
