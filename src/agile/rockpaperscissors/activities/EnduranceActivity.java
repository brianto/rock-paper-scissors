package agile.rockpaperscissors.activities;

import agile.rockpaperscissors.R;
import agile.rockpaperscissors.activities.VsComputerActivity.ThrowSelectionListener;
import agile.rockpaperscissors.dao.StatisticsDAO;
import agile.rockpaperscissors.model.Computer;
import agile.rockpaperscissors.model.Statistics;
import agile.rockpaperscissors.model.Throw;
import agile.rockpaperscissors.model.ThrowResult;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class EnduranceActivity extends Activity {
	public static final int ROUND_DURATION = 16; // seconds
	public static final int SEC_TO_MILLISEC = 1000; // 1000 msec = 1 sec
	
	private CountDownTimer timer;
	private Computer computer;
	
	// Results variables
	private int wins;
	private int losses;
	private int draws;
	private long remainingTime;
	private Throw computerSelection;
	private ThrowResult result;
	
	private TextView hud;
	
	private ImageButton rockA;
	private ImageButton paperA;
	private ImageButton scissorsA;
	
	private ImageButton rockB;
	private ImageButton paperB;
	private ImageButton scissorsB;
	
	private StatisticsDAO statisticsDAO;
	private Statistics statistics;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_endurance);
		
		this.computer = new Computer(PreferenceManager.getDefaultSharedPreferences(this));
		
		this.hud = (TextView) findViewById(R.id.hud);
		
		this.rockA = (ImageButton) findViewById(R.id.a_rock);
		this.paperA = (ImageButton) findViewById(R.id.a_paper);
		this.scissorsA = (ImageButton) findViewById(R.id.a_scissors);
		
		this.rockB = (ImageButton) findViewById(R.id.b_rock);
		this.paperB = (ImageButton) findViewById(R.id.b_paper);
		this.scissorsB = (ImageButton) findViewById(R.id.b_scissors);
		
		this.rockA.setOnClickListener(new ThrowSelectionListener(Throw.ROCK));
		this.paperA.setOnClickListener(new ThrowSelectionListener(Throw.PAPER));
		this.scissorsA.setOnClickListener(new ThrowSelectionListener(Throw.SCISSORS));
		
		this.rockB.setClickable(false);
		this.paperB.setClickable(false);
		this.scissorsB.setClickable(false);
	}
	
	@Override
	protected void onResume() {
		this.timer = new EnduranceTimer();
		this.timer.start();
		
		this.statisticsDAO = new StatisticsDAO(this);
		this.statisticsDAO.open();
		this.statistics = this.statisticsDAO.getOrCreate();
		
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		this.timer.cancel();
		
		this.statisticsDAO.update(this.statistics);
		this.statisticsDAO.close();
		
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.global, menu);
		return true;
	}
	
	public void onRockPaperScissorsSelected(Throw selection) {
		this.computer.playAgainst(selection);
		this.result = this.computer.getResult();
		this.computerSelection = this.computer.getSelection();
		
		this.statistics.update(this.result);
		
		switch (this.result) {
		case WIN: this.wins++; break;
		case LOSS: this.losses++; break;
		case DRAW: this.draws++; break;
		}
		
		this.colorizeResults();
		this.updateResults();
	}

	private void colorizeResults() {
		this.rockB.setBackgroundColor(Color.TRANSPARENT);
		this.paperB.setBackgroundColor(Color.TRANSPARENT);
		this.scissorsB.setBackgroundColor(Color.TRANSPARENT);
		
		int paint;
		switch (this.result) {
		case WIN: paint = Color.GREEN; break;
		case LOSS: paint = Color.RED; break;
		case DRAW: paint = Color.GRAY; break;
		default: paint = Color.TRANSPARENT; break;
		}
		
		switch (this.computerSelection) {
		case ROCK: this.rockB.setBackgroundColor(paint); break;
		case PAPER: this.paperB.setBackgroundColor(paint); break;
		case SCISSORS: this.scissorsB.setBackgroundColor(paint); break;
		default: break;
		}
	}
	
	private void updateResults() {
		double percentage;
		
		if (this.wins + this.losses + this.draws == 0) {
			percentage = 0;
		} else {
			percentage = 100 * this.wins / (double) (this.wins + this.losses + this.draws);
		}
		
		this.hud.setText(String.format(
				"W: %d\nD: %d\nL: %d\n%%: %03.2f\n\nRound: %ds",
				this.wins,
				this.draws,
				this.losses,
				percentage,
				Math.round(this.remainingTime / SEC_TO_MILLISEC)));
	}
	
	private void timerFinished() {
		this.remainingTime = 0;
		
		this.rockA.setClickable(false);
		this.paperA.setClickable(false);
		this.scissorsA.setClickable(false);
		
		this.updateResults();
	}
	
	private void timerUpdate(long remainingTime) {
		this.remainingTime = remainingTime;
		
		this.updateResults();
	}
	
	class ThrowSelectionListener implements OnClickListener {
		private Throw selection;
		
		public ThrowSelectionListener(Throw selection) {
			this.selection = selection;
		}

		@Override
		public void onClick(View v) {
			EnduranceActivity.this.onRockPaperScissorsSelected(this.selection);
		}
	}
	
	class EnduranceTimer extends CountDownTimer {
		public EnduranceTimer() {
			super(ROUND_DURATION * SEC_TO_MILLISEC, 1 * SEC_TO_MILLISEC);
		}

		@Override
		public void onFinish() {
			EnduranceActivity.this.timerFinished();
		}

		@Override
		public void onTick(long millisUntilFinished) {
			EnduranceActivity.this.timerUpdate(millisUntilFinished);
		}
	}
}
