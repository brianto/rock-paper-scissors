package agile.rockpaperscissors.activities;

import agile.rockpaperscissors.R;
import agile.rockpaperscissors.dao.StatisticsDAO;
import agile.rockpaperscissors.model.Statistics;
import agile.rockpaperscissors.model.Throw;
import agile.rockpaperscissors.model.ThrowResult;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class VsPlayerActivity extends Activity {
	private TextView result;
	
	private ImageButton rockA;
	private ImageButton paperA;
	private ImageButton scissorsA;
	
	private ImageButton rockB;
	private ImageButton paperB;
	private ImageButton scissorsB;
	
	private Throw throwA;
	private Throw throwB;
	
	private StatisticsDAO statisticsDAO;
	private Statistics statistics;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vs_player);
		
		this.rockA = (ImageButton) findViewById(R.id.a_rock);
		this.paperA = (ImageButton) findViewById(R.id.a_paper);
		this.scissorsA = (ImageButton) findViewById(R.id.a_scissors);
		
		this.rockB = (ImageButton) findViewById(R.id.b_rock);
		this.paperB = (ImageButton) findViewById(R.id.b_paper);
		this.scissorsB = (ImageButton) findViewById(R.id.b_scissors);
		
		this.rockA.setOnClickListener(new PlayerThrowListener(Throw.ROCK, Player.A));
		this.paperA.setOnClickListener(new PlayerThrowListener(Throw.PAPER, Player.A));
		this.scissorsA.setOnClickListener(new PlayerThrowListener(Throw.SCISSORS, Player.A));
		
		this.rockB.setOnClickListener(new PlayerThrowListener(Throw.ROCK, Player.B));
		this.paperB.setOnClickListener(new PlayerThrowListener(Throw.PAPER, Player.B));
		this.scissorsB.setOnClickListener(new PlayerThrowListener(Throw.SCISSORS, Player.B));
	}
	
	@Override
	public void onResume() {
		this.statisticsDAO = new StatisticsDAO(this);
		this.statisticsDAO.open();
		this.statistics = this.statisticsDAO.getOrCreate();
		
		super.onResume();
	}

	@Override
	public void onPause() {
		this.statisticsDAO.update(this.statistics);
		this.statisticsDAO.close();
		
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.global, menu);
		return true;
	}
	
	public void onRockPaperScissorsSelected(Throw selection, Player player) {
		switch (player) {
		case A: this.throwA = selection; break;
		case B: this.throwB = selection; break;
		default: break;
		}
		
		if (this.throwA != null && this.throwB != null) {
			this.updateWinner();
			
			this.throwA = null;
			this.throwB = null;
		}
	}
	
	private void updateWinner() {
		this.updateWinDisplay();
		
		ThrowResult result = this.throwA.resultAgainst(this.throwB);
		
		this.statistics.update(result);
		
		switch (result) {
		case WIN: updateAColors(ThrowResult.WIN); updateBColors(ThrowResult.LOSS); break;
		case LOSS: updateAColors(ThrowResult.LOSS); updateBColors(ThrowResult.WIN); break;
		case DRAW: updateAColors(ThrowResult.DRAW); updateBColors(ThrowResult.DRAW); break;
		}
	}
	
	private void updateAColors(ThrowResult result) {
		int color = resultColor(result);
		
		switch (this.throwA) {
		case ROCK: this.rockA.setBackgroundColor(color); break;
		case PAPER: this.paperA.setBackgroundColor(color); break;
		case SCISSORS: this.scissorsA.setBackgroundColor(color); break;
		}
	}
	
	private void updateBColors(ThrowResult result) {
		int color = resultColor(result);
		
		switch (this.throwB) {
		case ROCK: this.rockB.setBackgroundColor(color); break;
		case PAPER: this.paperB.setBackgroundColor(color); break;
		case SCISSORS: this.scissorsB.setBackgroundColor(color); break;
		}
	}
	
	private void updateWinDisplay() {
		this.rockA.setBackgroundColor(Color.TRANSPARENT);
		this.paperA.setBackgroundColor(Color.TRANSPARENT);
		this.scissorsA.setBackgroundColor(Color.TRANSPARENT);
		
		this.rockB.setBackgroundColor(Color.TRANSPARENT);
		this.paperB.setBackgroundColor(Color.TRANSPARENT);
		this.scissorsB.setBackgroundColor(Color.TRANSPARENT);
	}

	class PlayerThrowListener implements OnClickListener {
		private Throw selection;
		private Player player;
		
		public PlayerThrowListener(Throw selection, Player player) {
			this.selection = selection;
			this.player = player;
		}

		@Override
		public void onClick(View v) {
			VsPlayerActivity.this.onRockPaperScissorsSelected(this.selection, this.player);
		}
	}
	
	private static int resultColor(ThrowResult result) {
		switch (result) {
		case WIN: return Color.GREEN;
		case LOSS: return Color.RED;
		case DRAW: return Color.GRAY;
		}

		return Color.TRANSPARENT;
	}
}

enum Player {
	A, B;
}