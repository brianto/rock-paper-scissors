package agile.rockpaperscissors.activities;

import agile.rockpaperscissors.R;
import agile.rockpaperscissors.dao.StatisticsDAO;
import agile.rockpaperscissors.model.Statistics;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class StatisticsActivity extends Activity {
	
	private TextView wins;
	private TextView losses;
	private TextView draws;
	
	private Button reset;
	
	private StatisticsDAO statisticsDAO;
	private Statistics statistics;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statistics);
		
		this.wins = (TextView) this.findViewById(R.id.win_value);
		this.losses = (TextView) this.findViewById(R.id.loss_value);
		this.draws = (TextView) this.findViewById(R.id.draw_value);
		this.reset = (Button) this.findViewById(R.id.reset_statistics);
		
		this.reset.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				StatisticsActivity.this.resetStatistics();
			}
		});
	}
	
	@Override
	public void onResume() {
		this.statisticsDAO = new StatisticsDAO(this);
		this.statisticsDAO.open();
		
		this.statistics = this.statisticsDAO.getOrCreate();
		
		this.updateText();

		super.onResume();
	}
	
	@Override
	public void onPause() {
		this.statisticsDAO.close();

		super.onPause();
	}
	
	private void updateText() {
		this.wins.setText(Integer.toString(this.statistics.getWins()));
		this.losses.setText(Integer.toString(this.statistics.getLosses()));
		this.draws.setText(Integer.toString(this.statistics.getDraws()));
	}
	
	private void resetStatistics() {
		this.statisticsDAO.reset();
		this.statistics = this.statisticsDAO.getOrCreate();
		this.updateText();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.statistics, menu);
		return true;
	}
}
