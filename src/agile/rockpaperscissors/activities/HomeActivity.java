package agile.rockpaperscissors.activities;

import agile.rockpaperscissors.R;
import agile.rockpaperscissors.util.SystemUiHider;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class HomeActivity extends Activity {
	private Button playVsComputer;
	private Button playVsPlayer;
	private Button playEndurance;
	private Button settings;
	private Button statistics;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		this.playVsComputer = (Button) findViewById(R.id.play_vs_computer);
		this.playVsPlayer = (Button) findViewById(R.id.play_vs_player);
		this.playEndurance = (Button) findViewById(R.id.play_endurance);
		this.settings = (Button) findViewById(R.id.settings);
		this.statistics = (Button) findViewById(R.id.statistics);

		this.playVsComputer.setOnClickListener(new SwitchActivityListener(this, VsComputerActivity.class));
		this.playVsPlayer.setOnClickListener(new SwitchActivityListener(this, VsPlayerActivity.class));
		this.playEndurance.setOnClickListener(new SwitchActivityListener(this, EnduranceActivity.class));
		this.settings.setOnClickListener(new SwitchActivityListener(this, SettingsActivity.class));
		this.statistics.setOnClickListener(new SwitchActivityListener(this, StatisticsActivity.class));
	}

	public class SwitchActivityListener implements OnClickListener {
		private HomeActivity context;
		private Class<? extends Activity> target;

		public SwitchActivityListener(HomeActivity context,
				Class<? extends Activity> target) {
			this.context = context;
			this.target = target;
		}

		@Override
		public void onClick(View v) {
			context.startActivity(new Intent(context, target));
		}
	}
}
