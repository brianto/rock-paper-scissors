package agile.rockpaperscissors.activities;

import agile.rockpaperscissors.R;
import agile.rockpaperscissors.dao.StatisticsDAO;
import agile.rockpaperscissors.model.Computer;
import agile.rockpaperscissors.model.Statistics;
import agile.rockpaperscissors.model.Throw;
import agile.rockpaperscissors.model.ThrowResult;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

public class VsComputerActivity extends Activity {

	private Computer computer;
	
	private TextView result;
	
	private ImageButton rockA;
	private ImageButton paperA;
	private ImageButton scissorsA;
	
	private ImageButton rockB;
	private ImageButton paperB;
	private ImageButton scissorsB;
	
	private StatisticsDAO statisticsDAO;
	private Statistics statistics;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vs_computer);
		
		this.computer = new Computer(PreferenceManager.getDefaultSharedPreferences(this));
		
		this.result = (TextView) findViewById(R.id.result);
		
		this.rockA = (ImageButton) findViewById(R.id.a_rock);
		this.paperA = (ImageButton) findViewById(R.id.a_paper);
		this.scissorsA = (ImageButton) findViewById(R.id.a_scissors);
		
		this.rockB = (ImageButton) findViewById(R.id.b_rock);
		this.paperB = (ImageButton) findViewById(R.id.b_paper);
		this.scissorsB = (ImageButton) findViewById(R.id.b_scissors);
		
		this.rockA.setOnClickListener(new ThrowSelectionListener(Throw.ROCK));
		this.paperA.setOnClickListener(new ThrowSelectionListener(Throw.PAPER));
		this.scissorsA.setOnClickListener(new ThrowSelectionListener(Throw.SCISSORS));
		
		this.rockB.setClickable(false);
		this.paperB.setClickable(false);
		this.scissorsB.setClickable(false);
	}
	
	@Override
	public void onResume() {
		this.statisticsDAO = new StatisticsDAO(this);
		this.statisticsDAO.open();
		this.statistics = this.statisticsDAO.getOrCreate();
		
		super.onResume();
	}

	@Override
	public void onPause() {
		this.statisticsDAO.update(this.statistics);
		this.statisticsDAO.close();
		
		super.onPause();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.global, menu);
		return true;
	}
	
	public void onRockPaperScissorsSelected(Throw selection) {
		this.computer.playAgainst(selection);
		ThrowResult result = this.computer.getResult();
		Throw computerSelection = this.computer.getSelection();
		
		this.statistics.update(result);
		
		this.result.setText(String.format("You: %s\nCPU: %s\n\nResult: %s", selection, computerSelection, result));
		
		this.colorizeResults(result, computerSelection);
	}
	
	private void colorizeResults(ThrowResult result, Throw computerSelection) {
		this.rockB.setBackgroundColor(Color.TRANSPARENT);
		this.paperB.setBackgroundColor(Color.TRANSPARENT);
		this.scissorsB.setBackgroundColor(Color.TRANSPARENT);
		
		int paint;
		switch (result) {
		case WIN: paint = Color.GREEN; break;
		case LOSS: paint = Color.RED; break;
		case DRAW: paint = Color.GRAY; break;
		default: paint = Color.TRANSPARENT; break;
		}
		
		switch (computerSelection) {
		case ROCK: this.rockB.setBackgroundColor(paint); break;
		case PAPER: this.paperB.setBackgroundColor(paint); break;
		case SCISSORS: this.scissorsB.setBackgroundColor(paint); break;
		default: break;
		}
	}
	
	class ThrowSelectionListener implements OnClickListener {
		private Throw selection;
		
		public ThrowSelectionListener(Throw selection) {
			this.selection = selection;
		}

		@Override
		public void onClick(View v) {
			VsComputerActivity.this.onRockPaperScissorsSelected(this.selection);
		}
	}
}
