package agile.rockpaperscissors.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLHelper extends SQLiteOpenHelper {
	private static final int VERSION = 1;
	
	public static final String DATABASE = "rock-paper-scissors.sqlite";
	public static final String TABLE_STATISTICS = "statistics";
	
	public static final String COLUMN_ID = "id";
	public static final String COLUMN_WINS = "wins";
	public static final String COLUMN_LOSSES = "losses";
	public static final String COLUMN_DRAWS = "draws";
	public static final String COLUMN_STATISTIC_DEFINITION = "INTEGER NOT NULL";
	
	public static final String[] COLUMNS = {
		COLUMN_ID,
		COLUMN_WINS,
		COLUMN_LOSSES,
		COLUMN_DRAWS
	};
	
	private static final String SQL_DROP_TABLE = String.format(
			"DROP TABLE IF EXISTS %s", TABLE_STATISTICS);
	private static final String SQL_CREATE_TABLE = String.format(
			"CREATE TABLE %s (%s, %s, %s, %s);",
			TABLE_STATISTICS,
			String.format("%s %s", COLUMN_ID, "INTEGER PRIMARY KEY AUTOINCREMENT"),
			String.format("%s %s", COLUMN_WINS, COLUMN_STATISTIC_DEFINITION),
			String.format("%s %s", COLUMN_LOSSES, COLUMN_STATISTIC_DEFINITION),
			String.format("%s %s", COLUMN_DRAWS, COLUMN_STATISTIC_DEFINITION));

	public SQLHelper(Context context) {
		super(context, DATABASE, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(SQLHelper.class.getName(), String.format(
				"Upgrading db from v%s to v%s", oldVersion, newVersion));
		
	    db.execSQL(SQL_DROP_TABLE);
	    this.onCreate(db);
    }

}
