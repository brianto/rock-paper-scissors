package agile.rockpaperscissors.model;

public enum ThrowResult {
	WIN,
	LOSS,
	DRAW;
}
