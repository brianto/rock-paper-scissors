package agile.rockpaperscissors.model;

public enum Difficulty {
	EASY(0.7, 0.2),
	HARD(0.4, 0.4),
	LUNATIC(0.2, 0.6);
	
	private double win;
	private double loss;
	
	private Difficulty(double win, double loss) {
		if (win + loss >= 1) {
			throw new IllegalArgumentException("Sum of win and loss cannot be >= 1");
		}
		
		this.win = win;
		this.loss = loss;
	}
	
	public double getWin() {
		return this.win;
	}
	
	public double getLose() {
		return this.loss;
	}
	
	public ThrowResult randomThrowResult() {
		double indicator = Math.random();
		
		if (indicator < this.win) {
			return ThrowResult.WIN;
		} else if (this.win < indicator && indicator < this.win + this.loss) {
			return ThrowResult.LOSS;
		} else {
			return ThrowResult.DRAW;
		}
	}
}
