package agile.rockpaperscissors.model;

public class Statistics {

	private long id;
	private int wins;
	private int losses;
	private int draws;
	
	public Statistics() { }
	
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public int getWins() {
		return this.wins;
	}

	public void setWins(int wins) {
		this.wins = wins;
	}

	public int getLosses() {
		return this.losses;
	}

	public void setLosses(int losses) {
		this.losses = losses;
	}

	public int getDraws() {
		return this.draws;
	}

	public void setDraws(int draws) {
		this.draws = draws;
	}
	
	public void update(ThrowResult result) {
		switch (result) {
		case DRAW:
			this.draws++;
			break;
		case LOSS:
			this.losses++;
			break;
		case WIN:
			this.wins++;
			break;
		default:
			break;
		}
	}
	
	@Override
	public String toString() {
		return String.format("[w: %d, l: %d, d: %d]",
				this.wins, this.losses, this.draws);
	}
}
