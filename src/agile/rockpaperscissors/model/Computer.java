package agile.rockpaperscissors.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import android.content.SharedPreferences;
import android.util.Log;

public class Computer {

	private SharedPreferences preferences;
	private Throw selection;
	private ThrowResult result;
	
	public Computer(SharedPreferences preferences) {
		this.preferences = preferences;
	}
	
	public void playAgainst(Throw playerSelection) {
		this.result = this.nextResult();
		this.selection = this.nextSelection(playerSelection, this.result);
	}
	
	/**
	 * TODO replace implementation using some preferences
	 * @return
	 */
	private ThrowResult nextResult() {
		String preference = this.preferences.getString("difficulty", Difficulty.EASY.toString());
		Difficulty difficulty = Difficulty.valueOf(preference);
		
		return difficulty.randomThrowResult();
	}
	
	private Throw nextSelection(Throw playerSelection, ThrowResult result) {
		Set<Throw> choices = playerSelection.withOutcome(result);
		List<Throw> choicesList = new ArrayList<Throw>(choices);
		Collections.shuffle(choicesList);
		
		return choicesList.get(0);
	}

	public ThrowResult getResult() {
		return this.result;
	}

	public Throw getSelection() {
		return this.selection;
	}
}
