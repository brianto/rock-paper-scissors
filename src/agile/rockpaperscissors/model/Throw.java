package agile.rockpaperscissors.model;

import java.util.HashSet;
import java.util.Set;

public enum Throw {
	ROCK(new Rock()),
	PAPER(new Paper()),
	SCISSORS(new Scissors());
	
	public Winnable winnable;
	
	private Throw(Winnable winnable) {
		this.winnable = winnable;
	}
	
	public ThrowResult resultAgainst(Throw other) {
		return this.winnable.resultAgainst(other);
	}
	
	public Set<Throw> losingMatchups() {
		return this.withOutcome(ThrowResult.LOSS);
	}
	
	public Set<Throw> winningMatchups() {
		return this.withOutcome(ThrowResult.WIN);
	}
	
	public Set<Throw> drawMatchups() {
		return this.withOutcome(ThrowResult.DRAW);
	}
	
	public Set<Throw> withOutcome(ThrowResult result) {
		Throw[] values = Throw.values();
		Set<Throw> matching = new HashSet<Throw>(values.length);
		
		for (Throw t : values) {
			if (this.winnable.resultAgainst(t).equals(result)) {
				matching.add(t);
			}
		}
		
		return matching;
	}
}

abstract class Winnable {
	abstract ThrowResult resultAgainst(Throw other);
	
	public boolean winsAgainst(Throw other) {
		return this.resultAgainst(other).equals(ThrowResult.WIN);
	}
	
	public boolean losesAgainst(Throw other) {
		return this.resultAgainst(other).equals(ThrowResult.LOSS);
	}
}

class Rock extends Winnable {
	@Override
	public ThrowResult resultAgainst(Throw other) {
		switch (other) {
			case ROCK: return ThrowResult.DRAW;
			case PAPER: return ThrowResult.LOSS;
			case SCISSORS: return ThrowResult.WIN;
		}
		
		throw new IllegalArgumentException("derp");
	}
}

class Paper extends Winnable {
	@Override
	public ThrowResult resultAgainst(Throw other) {
		switch (other) {
			case ROCK: return ThrowResult.WIN;
			case PAPER: return ThrowResult.DRAW;
			case SCISSORS: return ThrowResult.LOSS;
		}
		
		throw new IllegalArgumentException("derp");
	}
}

class Scissors extends Winnable {
	@Override
	public ThrowResult resultAgainst(Throw other) {
		switch (other) {
			case ROCK: return ThrowResult.LOSS;
			case PAPER: return ThrowResult.WIN;
			case SCISSORS: return ThrowResult.DRAW;
		}
		
		throw new IllegalArgumentException("derp");
	}
}