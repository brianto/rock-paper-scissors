package agile.rockpaperscissors.dao;

import agile.rockpaperscissors.model.Statistics;
import agile.rockpaperscissors.util.SQLHelper;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Note that Statistics is a singleton object, only one row should exist at any time.
 */
public class StatisticsDAO {
	private SQLiteDatabase db;
	private SQLHelper helper;
	
	public StatisticsDAO(Context context) {
		this.helper = new SQLHelper(context);
	}
	
	public void open() throws SQLException {
		this.db = this.helper.getReadableDatabase();
	}
	
	public void close() {
		this.helper.close();
	}
	
	public void update(Statistics statistics) {
		ContentValues values = new ContentValues();
		values.put(SQLHelper.COLUMN_WINS, statistics.getWins());
		values.put(SQLHelper.COLUMN_LOSSES, statistics.getLosses());
		values.put(SQLHelper.COLUMN_DRAWS, statistics.getDraws());
		
		this.db.update(
				SQLHelper.TABLE_STATISTICS,
				values,
				String.format("%s = %d", SQLHelper.COLUMN_ID, statistics.getId()),
				null);
	}
	
	public Statistics create() {
		ContentValues values = new ContentValues();
		values.put(SQLHelper.COLUMN_WINS, 0);
		values.put(SQLHelper.COLUMN_LOSSES, 0);
		values.put(SQLHelper.COLUMN_DRAWS, 0);
		
	    long id = this.db.insert(SQLHelper.TABLE_STATISTICS, null, values);
	    
	    Cursor cursor = this.db.query(
	    		SQLHelper.TABLE_STATISTICS,
	    		SQLHelper.COLUMNS,
	    		String.format("%s = %d", SQLHelper.COLUMN_ID, id),
	    		null, null, null, null);
	    
	    cursor.moveToFirst();
	    
	    Statistics target = read(cursor);
	    
	    cursor.close();
	    
	    return target;
    }
	
	public Statistics getOrCreate() {
		Statistics statistics = null;
		
		Cursor cursor = this.db.query(
				SQLHelper.TABLE_STATISTICS,
				SQLHelper.COLUMNS,
				"",
				null, null, null, null);
		
		if (cursor.getCount() == 0) {
			this.create();
		}
		
		cursor.moveToFirst();
		
		statistics = this.read(cursor);

		if (!cursor.isLast()) {
			cursor.close();
			throw new IllegalStateException("There should only be one statistics object available, ever.");
		}
		
		cursor.close();
		
		return statistics;
	}
	
	public Statistics read(Cursor cursor) {
		Statistics statistics = new Statistics();
		
		statistics.setId(cursor.getLong(0));
		statistics.setWins(cursor.getInt(1));
		statistics.setLosses(cursor.getInt(2));
		statistics.setDraws(cursor.getInt(3));
		
		return statistics;
	}
	
	public void reset() {
		Statistics statistics = this.getOrCreate();
		statistics.setWins(0);
		statistics.setLosses(0);
		statistics.setDraws(0);
		
		this.update(statistics);
	}
}
